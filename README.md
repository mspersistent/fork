# Muffin Dev for NodeJS - Fork

This utility is meant for initializing fork repositories from Git repositories that may not be on the same platform.

## Installation

```bash
npm install -g @muffin-dev/fork
```

## Usage

### Initialize a fork repository

Use the `fork init` command to initialize new fork repository.

```bash
fork init <original-url> <fork-url> [--originalBranch <name>] [--forkBranch <name>] [--tmpBranch <name>] [--originalRemote <name>] [--forkRemote <name>] [-p, --push]
```

#### Arguments

- **`original-url`**: The URL of the original repository (should end with `*.git`)
- **`fork-url`**: The URL of your own repository where the original one will be forked (should end with `*.git`)

#### Options

- **`--originalBranch`**: The name of the branch in the original repository from which you want your fork repository to start (default: `main`).
- **`--forkBranch`**: The name of the branch in your own repository where the original branch will be merged (default: `main`).
- **`--tmpBranch`**: The name of the local branch that will handle the original branch fork before being merged (default: `original/main`).
- **`--originalRemote`**: The name of the remote to the original repository (default: `upstream`).
- **`--forkRemote`**: The name of the remote to your own repository (default: `origin`).
- **`-p, --push`**: If enabled, your new fork repository will be pushed automatically at the end of the operation.

---

Copyright Muffin Dev © 2022