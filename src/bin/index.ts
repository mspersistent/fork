#!/usr/bin/env node
import { Command } from 'commander';

import InitCommand from './init';

const program = new Command('fork');

InitCommand(program);

program.parse(process.argv);